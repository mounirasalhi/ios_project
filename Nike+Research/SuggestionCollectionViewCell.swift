//
//  SuggestionCollectionViewCell.swift
//  Nike+Research
//
//  Created by Mezri Abdealziz on 4/14/20.
//  Copyright © 2020 Developers Academy. All rights reserved.
//

import UIKit

class SuggestionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var image: UIImage! {
        didSet {
            self.imageView.image = image
            self.setNeedsLayout()
        }
    }
    
}
















