//
//  ShoeImageViewController.swift
//  Nike+Research
//
//  Created by Mezri Abdealziz on 4/12/20.
//  Copyright © 2020 Developers Academy. All rights reserved.
//

import UIKit

class ShoeImageViewController: UIViewController
{
    @IBOutlet weak var imageView: UIImageView!
    
    var image: UIImage? {
        didSet {
            self.imageView?.image = image
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imageView.image = image
    }

}
