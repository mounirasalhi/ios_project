//
//  ShoeImagesHeaderViewController.swift
//  Nike+Research
//
//  Created by Mezri Abdealziz on 4/14/20.
//  Copyright © 2020 Developers Academy. All rights reserved.
//

import UIKit

class ShoeImagesHeaderView: UIView
{
    @IBOutlet weak var pageControl: UIPageControl!
}

extension ShoeImagesHeaderView : ShoeImagesPageViewControllerDelegate
{
    func setupPageController(numberOfPages: Int)
    {
        pageControl.numberOfPages = numberOfPages
    }
    
    func turnPageController(to index: Int)
    {
        pageControl.currentPage = index
    }
}











